var express = require("express");
var bodyParser = require("body-parser");
var cors = require('cors');

var app = express();
app.use(cors());


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(express.static(__dirname+'/uploads'));
app.use(express.static(__dirname+'/images'));

var port = 3002;
var carts = require("./routing/carts");
app.use('/api', carts);

app.listen(port, function(){
    console.log('listening on port '+port);
});