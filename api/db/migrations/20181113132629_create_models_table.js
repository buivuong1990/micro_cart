
exports.up = function(knex, Promise) {
    return knex.schema.createTable('models', function(t){
        t.increments('id').primary();
        t.string('name');
        t.integer('category_id');
        t.integer('brand_id');
        t.integer('color_id');
        t.integer('capacity_id');
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('models');
};