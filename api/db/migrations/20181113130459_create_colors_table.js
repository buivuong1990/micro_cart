
exports.up = function(knex, Promise) {
    return knex.schema.createTable('colors', function(t){
        t.increments('id').primary();
        t.string('name');
        t.string('code');
    });
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('colors');
};
