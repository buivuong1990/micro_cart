var express = require("express");
var knex = require("../config/knex");
var multer  = require('multer')
var router = express.Router();
var path = require('path');
var fs = require('fs');

router.post('/user/cart', function(req, res){
    var device_id = req.body.device_id;
    var available_id = req.body.available_id;
    var email = req.body.email;
    var type = req.body.type;

    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            var now = knex.fn.now();

            return knex('carts')
            .insert({
                device_id: device_id,
                available_id: available_id,
                user_id: user_id,
                type: type,
                created_at: now
            })
        }else{
            res.json({status: 500});
        }
    })
    .then(function(created){
        if(created.length > 0)
            res.json({status: 200, data: created[0]});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/user/cart/email', function(req, res){
    var email = req.body.email;
    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex.raw(
                `SELECT carts.id as id,
                    carts.created_at as created_at,
                    availables.sale_price as sale_price,
                    availables.exchange_price as exchange_price,
                    availables.exchange_name as exchange_name,
                    availables.type as type,
                    devs.device_name as device_name,
                    devs.url as thumb,
                    devs.id as device_id,
                    devs.brand_name as brand_name,
                    devs.color_name as color_name,
                    devs.capacity_name as capacity_name,
                    devs.received_email as received_email,
                    carts.type as cart_type,
                    proposals.status as proposal_status,
                    proposals.exchange_proposal_price as exchange_proposal_price,
                    proposals.sale_proposal_price as sale_proposal_price,
                    proposals.id as proposal_id,
                    proposals.device_name as exchange_proposal_real_device,
                    proposals.device_id as exchange_proposal_real_device_id,
                    proposals.thumb as exchange_proposal_real_device_thumb,
                    proposals.recommend_device as proposal_recommend_device
                FROM carts
                LEFT OUTER JOIN (
                    SELECT device_availables.id, device_availables.sale_price, device_availables.exchange_price,
                        device_availables.type, models.name as exchange_name
                    FROM device_availables
                    LEFT OUTER JOIN models ON device_availables.model_id = models.id
                ) as availables ON carts.available_id = availables.id
                INNER JOIN (
                    SELECT devices.id, imeiss.device_name, device_images.url, imeiss.brand_name, imeiss.color_name,
                        imeiss.capacity_name, users.email as received_email
                    FROM devices
                    INNER JOIN (
                        SELECT imeis.imei, models.name as device_name, models.brand_name,
                            models.color_name, models.capacity_name
                        FROM imeis
                        INNER JOIN (
                            SELECT models.id, models.name, categories.name as category_name, brands.name as brand_name,
                                colors.name as color_name, capacities.name as capacity_name, rams.name as ram_name
                            FROM models
                            INNER JOIN categories ON models.category_id = categories.id
                            INNER JOIN brands ON models.brand_id = brands.id
                            INNER JOIN colors ON models.color_id = colors.id
                            INNER JOIN capacities ON models.capacity_id = capacities.id
                            INNER JOIN rams ON models.ram_id = rams.id
                        ) AS models ON imeis.model_id = models.id
                    ) as imeiss ON imeiss.imei = devices.imei
                    INNER JOIN users ON users.id=devices.user_id
                    LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                    WHERE devices.user_id <> `+user_id+`
                ) as devs ON carts.device_id = devs.id
                LEFT OUTER JOIN (
                    SELECT proposals.id, proposals.cart_id, proposals.exchange_proposal_device,
                        proposals.exchange_proposal_price, proposals.sale_proposal_price, proposals.status,
                        proposals.type, proposals.user_id, devices.device_name, devices.thumb,
                        proposals.recommend_device,
                        devices.id as device_id
                    FROM proposals
                    LEFT OUTER JOIN (
                        SELECT devices.id, imeiss.device_name, imeiss.brand_name, imeiss.color_name,
                            imeiss.capacity_name, device_images.url as thumb
                        FROM devices
                        INNER JOIN (
                            SELECT imeis.imei, models.name as device_name, models.brand_name,
                                models.color_name, models.capacity_name
                            FROM imeis
                            INNER JOIN (
                                SELECT models.id, models.name, categories.name as category_name, brands.name as brand_name,
                                    colors.name as color_name, capacities.name as capacity_name, rams.name as ram_name
                                FROM models
                                INNER JOIN categories ON models.category_id = categories.id
                                INNER JOIN brands ON models.brand_id = brands.id
                                INNER JOIN colors ON models.color_id = colors.id
                                INNER JOIN capacities ON models.capacity_id = capacities.id
                                INNER JOIN rams ON models.ram_id = rams.id
                            ) AS models ON imeis.model_id = models.id
                        ) as imeiss ON imeiss.imei = devices.imei
                        LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                    ) as devices ON devices.id = proposals.exchange_proposal_device
                    WHERE proposals.user_id = `+user_id+`
                ) AS proposals ON proposals.cart_id=carts.id
                WHERE carts.user_id=`+user_id+`
                ORDER BY carts.created_at DESC`
            )
        }else{
            res.json({status: 500});
        }
    })
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.delete('/user/cart', function(req, res){
    var id = req.body.id;

    knex('carts')
    .where('id', id)
    .del()
    .then(function(deleted){
        knex.raw(
            `DELETE FROM proposals
            WHERE cart_id=`+id
        )
        .then(() => {
            res.json({status: 200, data: deleted});
        })
        .catch(() => {
            res.json({status: 200});
        })
    })
    .catch(function(error){
        res.json({status: 500});
    })
});
router.get('/user/cart/:id', function(req, res){
    var id = req.params.id;
    knex.raw(
        `SELECT carts.id as id,
            carts.created_at as created_at,
            availables.sale_price as sale_price,
            availables.exchange_price as exchange_price,
            availables.exchange_name as exchange_name,
            availables.type as type,
            devs.device_name as device_name,
            devs.url as thumb,
            devs.id as device_id,
            devs.brand_name as brand_name,
            devs.color_name as color_name,
            devs.capacity_name as capacity_name,
            carts.type as cart_type
        FROM carts
        LEFT OUTER JOIN (
            SELECT device_availables.id, device_availables.sale_price, device_availables.exchange_price,
                device_availables.type, models.name as exchange_name
            FROM device_availables
            LEFT OUTER JOIN models ON device_availables.model_id = models.id
        ) as availables ON carts.available_id = availables.id
        INNER JOIN (
            SELECT devices.id, imeiss.device_name, device_images.url, imeiss.brand_name, imeiss.color_name,
                imeiss.capacity_name
            FROM devices
            INNER JOIN (
                SELECT imeis.imei, models.name as device_name, models.brand_name,
                    models.color_name, models.capacity_name
                FROM imeis
                INNER JOIN (
                    SELECT models.id, models.name, categories.name as category_name, brands.name as brand_name,
                        colors.name as color_name, capacities.name as capacity_name, rams.name as ram_name
                    FROM models
                    INNER JOIN categories ON models.category_id = categories.id
                    INNER JOIN brands ON models.brand_id = brands.id
                    INNER JOIN colors ON models.color_id = colors.id
                    INNER JOIN capacities ON models.capacity_id = capacities.id
                    INNER JOIN rams ON models.ram_id = rams.id
                ) AS models ON imeis.model_id = models.id
            ) as imeiss ON imeiss.imei = devices.imei
            LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
        ) as devs ON carts.device_id = devs.id
        WHERE carts.id=`+id
    )
    .then(function(rows){
        if(rows.length > 0){
            res.json({data: rows[0], status: 200});
        }else{
            res.json({status: 400});
        }
    })
    .catch(function(error){
        res.status(500).json();
    });
});
router.post('/proposal/add', function(req, res){
    var cart_id = req.body.cart_id;
    var type = req.body.type;
    var sale_proposal_price = null;
    var exchange_proposal_price = null;
    var exchange_proposal_device = null;
    var status = 'created';
    var email = req.body.email;
    var created_at = knex.fn.now();

    if(type === 1){
        sale_proposal_price = req.body.sale_proposal_price;
    }else if(type === 2){
        exchange_proposal_price = req.body.exchange_proposal_price;
        exchange_proposal_device = req.body.exchange_proposal_device;
    }

    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex('proposals')
            .insert({
                cart_id: cart_id,
                user_id: user_id,
                type: type,
                sale_proposal_price: sale_proposal_price,
                exchange_proposal_price: exchange_proposal_price,
                exchange_proposal_device: exchange_proposal_device,
                status: status,
                created_at: created_at
            })
        }else{
            res.json({status: 500});
        }
    })
    .then(function(created){
        if(created.length > 0)
            res.json({status: 200, data: created[0]});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.delete('/proposal/cancel', function(req, res){
    var id = req.body.id;
    knex('proposals')
    .where('id', id)
    .del()
    .then(function(deleted){
        res.json({status: 200, data: deleted});
    })
    .catch(function(error){
        res.status(500).json();
    });
});
router.post('/proposal/edit', function(req, res){
    var cart_id = req.body.cart_id;
    var type = req.body.type;
    var sale_proposal_price = null;
    var exchange_proposal_price = null;
    var exchange_proposal_device = null;
    var status = req.body.status ? req.body.status : 'updated';
    var email = req.body.email;
    var id = req.body.id;

    if(type === 1){
        sale_proposal_price = req.body.sale_proposal_price;

    }else if(type === 2){
        exchange_proposal_price = req.body.exchange_proposal_price;
        exchange_proposal_device = req.body.exchange_proposal_device;
    }

    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex('proposals')
            .update({
                cart_id: cart_id,
                user_id: user_id,
                type: type,
                sale_proposal_price: sale_proposal_price,
                exchange_proposal_price: exchange_proposal_price,
                exchange_proposal_device: exchange_proposal_device,
                status: status
            })
            .where('id', id)
        }else{
            return knex('proposals')
            .update({
                type: type,
                sale_proposal_price: sale_proposal_price,
                exchange_proposal_price: exchange_proposal_price,
                exchange_proposal_device: exchange_proposal_device,
                status: status
            })
            .where('id', id)
        }
    })
    .then(function(updated){
        if(updated)
            res.json({status: 200, data: updated});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/proposal/received/list', function(req, res){
    var email = req.body.email;
    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex.raw(
                `SELECT proposals.id AS id,
                    proposals.sale_proposal_price,
                    proposals.exchange_proposal_price,
                    proposals.status as proposal_status,
                    proposals.type as proposal_type,
                    carts.device_name as device_name,
                    carts.brand_name as brand_name,
                    carts.color_name as color_name,
                    carts.capacity_name as capacity_name,
                    carts.thumb
                FROM proposals
                INNER JOIN (
                    SELECT carts.id, devices.device_name AS device_name,
                        devices.brand_name AS brand_name,
                        devices.color_name AS color_name,
                        devices.capacity_name AS capacity_name,
                        devices.thumb
                    FROM carts
                    INNER JOIN (
                        SELECT devices.id, imeiss.device_name, imeiss.brand_name, imeiss.color_name, imeiss.capacity_name,
                            device_images.url as thumb
                        FROM devices
                        INNER JOIN (
                            SELECT imeis.imei, models.name as device_name, models.brand_name,
                                models.color_name, models.capacity_name
                            FROM imeis
                            INNER JOIN (
                                SELECT models.id, models.name, categories.name as category_name, brands.name as brand_name,
                                    colors.name as color_name, capacities.name as capacity_name, rams.name as ram_name
                                FROM models
                                INNER JOIN categories ON models.category_id = categories.id
                                INNER JOIN brands ON models.brand_id = brands.id
                                INNER JOIN colors ON models.color_id = colors.id
                                INNER JOIN capacities ON models.capacity_id = capacities.id
                                INNER JOIN rams ON models.ram_id = rams.id
                            ) AS models ON imeis.model_id = models.id
                        ) AS imeiss ON imeiss.imei = devices.imei
                        LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                        WHERE devices.user_id = `+user_id+`
                    ) AS devices ON carts.device_id = devices.id
                ) AS carts ON proposals.cart_id = carts.id
                ORDER BY proposals.created_at DESC`
            )
        }else{
            res.json({status: 500});
        }
    })
    .then(function(list){
        res.json({status: 200, data: list});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.get('/proposal/received/detail/:id/:email', function(req, res){
    var id = req.params.id;
    var email = req.params.email;
    knex('users').select('id').where({'email': email})
    .then(function(rows){
        if(rows.length > 0){
            var user_id = rows[0].id;
            return knex
            .raw(
                `
                SELECT proposals.id AS id,
                    proposals.sale_proposal_price,
                    proposals.exchange_proposal_price,
                    proposals.status as proposal_status,
                    proposals.type as proposal_type,
                    proposals.user_id as user_id,
                    proposals.recommend_device,
                    users.email as received_email,
                    carts.device_name as device_name,
                    carts.brand_name as brand_name,
                    carts.color_name as color_name,
                    carts.capacity_name as capacity_name,
                    carts.thumb,
                    carts.id as cart_id,
                    devices.id as exchange_device_id,
                    devices.device_name as exchange_device_name,
                    devices.thumb as exchange_device_thumb,
                    devices.brand_name as exchange_device_brand_name,
                    devices.color_name as exchange_device_color_name,
                    devices.capacity_name as exchange_device_capacity_name
                FROM proposals
                INNER JOIN (
                    SELECT carts.id, devices.device_name AS device_name,
                        devices.brand_name AS brand_name,
                        devices.color_name AS color_name,
                        devices.capacity_name AS capacity_name,
                        devices.thumb
                    FROM carts
                    INNER JOIN (
                        SELECT devices.id, imeiss.device_name, imeiss.brand_name, imeiss.color_name, imeiss.capacity_name,
                            device_images.url as thumb
                        FROM devices
                        INNER JOIN (
                            SELECT imeis.imei, models.name as device_name, models.brand_name,
                                models.color_name, models.capacity_name
                            FROM imeis
                            INNER JOIN (
                                SELECT models.id, models.name, categories.name as category_name, brands.name as brand_name,
                                    colors.name as color_name, capacities.name as capacity_name, rams.name as ram_name
                                FROM models
                                INNER JOIN categories ON models.category_id = categories.id
                                INNER JOIN brands ON models.brand_id = brands.id
                                INNER JOIN colors ON models.color_id = colors.id
                                INNER JOIN capacities ON models.capacity_id = capacities.id
                                INNER JOIN rams ON models.ram_id = rams.id
                            ) AS models ON imeis.model_id = models.id
                        ) AS imeiss ON imeiss.imei = devices.imei
                        LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                        WHERE devices.user_id = `+user_id+`
                    ) AS devices ON carts.device_id = devices.id
                ) AS carts ON proposals.cart_id = carts.id
                LEFT OUTER JOIN (
                    SELECT devices.id, imeiss.device_name, imeiss.brand_name, imeiss.color_name,
                        imeiss.capacity_name, device_images.url as thumb
                    FROM devices
                    INNER JOIN (
                        SELECT imeis.imei, models.name as device_name, models.brand_name,
                            models.color_name, models.capacity_name
                        FROM imeis
                        INNER JOIN (
                            SELECT models.id, models.name, categories.name as category_name, brands.name as brand_name,
                                colors.name as color_name, capacities.name as capacity_name, rams.name as ram_name
                            FROM models
                            INNER JOIN categories ON models.category_id = categories.id
                            INNER JOIN brands ON models.brand_id = brands.id
                            INNER JOIN colors ON models.color_id = colors.id
                            INNER JOIN capacities ON models.capacity_id = capacities.id
                            INNER JOIN rams ON models.ram_id = rams.id
                        ) AS models ON imeis.model_id = models.id
                    ) as imeiss ON imeiss.imei = devices.imei
                    LEFT OUTER JOIN device_images ON device_images.device_id = devices.id AND device_images.checked = 1
                ) as devices ON devices.id = proposals.exchange_proposal_device
                INNER JOIN users ON users.id = proposals.user_id
                WHERE proposals.id = `+id
            )
        }
    })
    .then(function(rows){
        if(rows.length > 0){
            res.json({data: rows[0], status: 200});
        }else{
            res.json({status: 400});
        }
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/proposal/recommend/add', function(req, res){
    var id = req.body.id;
    var recommend_device = req.body.recommend_device;

    knex('proposals')
    .update({
        recommend_device: recommend_device
    })
    .where('id', id)
    .then(function(updated){
        if(updated)
            res.json({status: 200, data: updated});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/proposal/recommend/delete', function(req, res){
    var id = req.body.id;
    var recommend_device = null;

    knex('proposals')
    .update({
        recommend_device: recommend_device
    })
    .where('id', id)
    .then(function(updated){
        if(updated)
            res.json({status: 200, data: updated});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});
router.post('/proposal/updateStatus', function(req, res){
    var id = req.body.id;
    var status = req.body.status;

    knex('proposals')
    .update({
        status: status
    })
    .where('id', id)
    .then(function(updated){
        if(updated)
            res.json({status: 200, data: updated});
        else
            res.json({status: 500});
    })
    .catch(function(error){
        res.status(500).json();
    })
});module.exports = router;